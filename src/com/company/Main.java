package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        Scanner sc=new Scanner(System.in);

        String path_deliveries="/home/ajay_modi/Downloads/archive/deliveries.csv";
        String path_matches="/home/ajay_modi/Downloads/archive/matches.csv";

        BufferedReader br=new BufferedReader(new FileReader(path_deliveries));
        BufferedReader br1=new BufferedReader(new FileReader(path_matches));

        String line="";

        String[] values_deliveries;
        String[] values_matches;

        Map<String,Integer> noOfMatchesPlayedPerYear=new HashMap<>();
        Map<String,Integer> noOfMatchesWonByTeams=new HashMap<>();
        Map<String,Map<String,Integer>> extraRunPerTeamPerYear=new HashMap<>();
        Map<String,String> matchId_matchYear=new HashMap<>();
        Map<String, Map<String,int[]>> topEconomicalBowlersPerYear =new HashMap<>();
        int l=0;

        while((line= br1.readLine())!=null)
        {

            values_matches=line.split(",");
            if(l==0)
            {
                l=1;
                continue;
            }

            if(values_matches[10].length()==0) values_matches[10]="no result";

            // start code for Number of matches played per year of all the years in IPL...

            if(noOfMatchesPlayedPerYear.containsKey(values_matches[1]))
            {
                int get=noOfMatchesPlayedPerYear.get(values_matches[1]);
                noOfMatchesPlayedPerYear.put(values_matches[1],get+1);
            }
            else
            {
                noOfMatchesPlayedPerYear.put(values_matches[1],1);
            }

            // start code for Number of matches won of all teams over all the years of IPL....

            if(noOfMatchesWonByTeams.containsKey(values_matches[10]))
            {
                int get=noOfMatchesWonByTeams.get(values_matches[10]);
                noOfMatchesWonByTeams.put(values_matches[10],get+1);
            }
            else
            {
                noOfMatchesWonByTeams.put(values_matches[10],1);
            }
            matchId_matchYear.put(values_matches[0],values_matches[1]);
        }
        System.out.println("Number of matches played per year of all the years in IPL :");
        System.out.println(noOfMatchesPlayedPerYear);
        System.out.println("");
        System.out.println("Number of matches won of all teams over all the years of IPL.");
        System.out.println(noOfMatchesWonByTeams);

        while((line= br.readLine())!=null)
        {
            values_deliveries=line.split(",");

            if(isNumeric(values_deliveries[16])) {

                String year = matchId_matchYear.get(values_deliveries[0]);

                if (extraRunPerTeamPerYear.containsKey(year)) {

                    Map<String, Integer> hm = extraRunPerTeamPerYear.get(year);

                    if (hm.containsKey(values_deliveries[2])) {
                        int extraRun = hm.get(values_deliveries[2]);
                        hm.put(values_deliveries[2], extraRun + Integer.valueOf(values_deliveries[16]));
                    }
                    else
                    {
                        hm.put(values_deliveries[2], Integer.valueOf(values_deliveries[16]));
                    }

                    extraRunPerTeamPerYear.put(year, hm);
                }
                else {

                    Map<String, Integer> hm = new HashMap<>();
                    hm.put(values_deliveries[2], Integer.valueOf(values_deliveries[16]));
                    extraRunPerTeamPerYear.put(year, hm);

                }

                if(topEconomicalBowlersPerYear.containsKey(year))
                {
                    Map<String, int[]> temp=topEconomicalBowlersPerYear.get(year);

                    if(temp.containsKey(values_deliveries[8]))
                    {
                        int arr[]=temp.get(values_deliveries[8]);
                        arr[0]+=1;
                        arr[1]+=Integer.valueOf(values_deliveries[17]);

                        if(Integer.valueOf(values_deliveries[10])>0 || Integer.valueOf(values_deliveries[13])>0)
                            arr[0]-=1;
                        temp.put(values_deliveries[8],arr);
                    }
                    else
                    {
                        int arr[]=new int[2];
                        arr[0]=Integer.valueOf(1);
                        arr[1]=Integer.valueOf(values_deliveries[17]);
                        temp.put(values_deliveries[8],arr);
                    }
                    topEconomicalBowlersPerYear.put(year,temp);
                }
                else
                {
                    Map<String,int[]> temp=new HashMap<>();
                    int arr[]=new int[2];
                    arr[0]=Integer.valueOf(values_deliveries[5]);
                    arr[1]=Integer.valueOf(values_deliveries[17]);
                    temp.put(values_deliveries[8],arr);
                    topEconomicalBowlersPerYear.put(year,temp);
                }
            }

        }

        System.out.println("");
        System.out.println("For the year 2016 get the extra runs conceded per team :");
        System.out.println(extraRunPerTeamPerYear.get(String.valueOf((2016))));

        System.out.println("");
        System.out.println("For the year, get the top economical bowlers :");
        System.out.println("Enter year from which you want top Economical bowlers: ");
        String year=sc.next();
        ArrayList<String> al=new ArrayList<>();
        if(topEconomicalBowlersPerYear.containsKey(year))
        {
            String bestEconomyPlayer="";
            float min=Float.MAX_VALUE;

                Map<String,int[]> map=topEconomicalBowlersPerYear.get(year);

                for(Map.Entry<String,int[]> itr:map.entrySet())
                {
                    int a[]= itr.getValue();
                    float overs=a[0]/6;
                    //System.out.println("Player Name: "+itr.getKey()+", Overs : "+overs+", Runs : "+a[1]);
                    float economy=(float)a[1]/overs;

                    if(min>economy)
                    {
                        min=economy;
                        bestEconomyPlayer= itr.getKey();
                        al.clear();
                        al.add("Player Name: "+bestEconomyPlayer+", Economy: "+min);
                    }
                    else if(min==economy)
                        al.add("Player Name: "+itr.getKey()+", Economy: "+min);
                }
            System.out.println("");

            System.out.println(al);
        }
        else
        {
            System.out.println("Please enter a valid year.....");
        }
        Map<String,int[]> map=topEconomicalBowlersPerYear.get("2015");
        for(Map.Entry<String,int[]> it:map.entrySet())
        {

            int[] a= it.getValue();
            System.out.println("");
            System.out.println("Bowlers name with runs and overs: ");
            System.out.print(it.getKey()+"--->  "+" Overs= "+(float)(a[0]/6)+" , runs: "+a[1]);
            System.out.println("");
        }

    }
    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
